import os
import json
import csv
import re
import codecs
import requests
import msal
import shutil
import itertools

from glob import glob
from logging import getLogger, basicConfig, DEBUG, INFO
from shutil import copyfile

from openpyxl import load_workbook

# TODO: Needs refactoring, lots of it

PEEK_SIZE = 96 * 1024

basicConfig(
    format="[{asctime}] [{levelname}] [line: {lineno}]: {message}",
    style="{",
    level=INFO,
)

logger = getLogger(__name__)

config_path = "/config/config.json"

if not os.path.exists(config_path):
    raise Exception("Configuration file not found")
with open(config_path) as conf_file:
    conf = json.load(conf_file)

if not "output_directory" in conf:
    conf["output_directory"] = "/data/out/tables"

if not os.path.exists(conf["output_directory"]):
    os.makedirs(conf["output_directory"])

skip_rows = conf.get("skip_rows", 0) or 0
skip_columns = conf.get("skip_columns", 0) or 0
table_width = conf.get("table_width")
blacklist = conf.get("blacklist", [])
whitelist = conf.get("whitelist", [])
filename_whitelist = conf.get("filename_whitelist", [])
filename_blacklist = conf.get("filename_blacklist", [])

session = requests.Session()


def download_xlsx(input_file, headers, merge=True):
    """Download xlsx input file to tmp folder

    Arguments:
        input_file {dict} --dict with information about input xlsx file from OneDrive

        headers {dict} -- headers for request
    """
    response = session.get(input_file["@microsoft.graph.downloadUrl"], headers=headers)
    name = input_file["name"]
    target = f"/tmp/merge/{name}" if merge else f"/tmp/convert/{name}"
    try:
        if response.status_code == 200:
            in_file = response.content
            with open(target, "wb") as fid:
                fid.write(in_file)
    except Exception as e:
        raise Exception(e)


def download_csv(input_file, headers):
    """Download csv input file to tmp folder

    Arguments:
        input_file {dict} --dict with information about input csv file from OneDrive

        headers {dict} -- headers for request
    """
    response = session.get(input_file["@microsoft.graph.downloadUrl"], headers=headers)
    name = input_file["name"]
    try:
        if response.status_code == 200:
            in_file = response.content
            with open(os.path.join("/tmp/dialect", name), "wb") as fid:
                fid.write(in_file)
    except Exception as e:
        raise Exception(e)


def get_list_id_by_name(site, list_name, subsite, headers):
    """Get list id by its name"""
    subsite = _subsite(subsite)
    url = f"https://graph.microsoft.com/v1.0/sites/{site}{subsite}/lists"
    logger.info(url)
    response = session.get(url, headers=headers)
    response.raise_for_status()
    lists = response.json().get("value", [])
    lid = [l["id"] for l in lists if l["name"] == list_name]
    if not lid:
        return None
    return lid[0]


def get_list_items(site, list_id, subsite, headers):
    """Get downloadable list items from list_id"""
    subsite = _subsite(subsite)
    response = session.get(
        f"https://graph.microsoft.com/v1.0/sites/{site}{subsite}/lists/{list_id}/items", headers=headers
    )
    response.raise_for_status()
    items = response.json().get("value", [])
    for item in items:
        response = session.get(
            f"https://graph.microsoft.com/v1.0/sites/{site}{subsite}/lists/{list_id}/items/{item['id']}/driveItem",
            headers=headers,
        )
        response.raise_for_status()
        driveitem = response.json()
        yield driveitem


def _iter_items(baseurl, item_ids, headers):
    for iid in item_ids:
        url = f"{baseurl}/{iid}"
        response = session.get(url, headers=headers)
        response.raise_for_status()
        yield response.json().get("fields", {})


def _subsite(subsite):
    if subsite.startswith(":/"):
        return subsite
    if subsite is None or subsite == "":
        return ""
    return f":/sites/{subsite}:"


def get_list_header(site, list_id, subsite, headers):
    subsite = _subsite(subsite)
    baseurl = f"https://graph.microsoft.com/v1.0/sites/{site}{subsite}/lists/{list_id}/columns"
    response = session.get(baseurl, headers=headers)
    response.raise_for_status()
    return [val["name"] for val in response.json().get("value", [])]


def download_list_as_table(site, list_id, subsite, headers, output):
    """Get list all items as a single table"""
    subsite = _subsite(subsite)
    baseurl = f"https://graph.microsoft.com/v1.0/sites/{site}{subsite}/lists/{list_id}/items"
    response = session.get(baseurl, headers=headers)
    response.raise_for_status()
    item_ids = [item["id"] for item in response.json().get("value", [])]
    data = _iter_items(baseurl, item_ids, headers)
    fieldnames = get_list_header(site, list_id, subsite, headers)
    logger.info("List columns: %s", ", ".join(fieldnames))
    with open(output, "w", encoding="utf-8") as fid:
        writer = csv.DictWriter(fid, fieldnames=fieldnames, dialect=csv.unix_dialect, extrasaction="ignore")
        writer.writeheader()
        writer.writerows(data)


def convert_dialect(input_file, output_file, input_encoding=None):
    """Ensure UNIX dialect for csv file

    Args:
        input_file (str): input path
        output_file (str): output path
        input_encoding (str): expected input encodint (output will always be utf8), default: utf-8
    """
    if input_encoding is None:
        input_encoding = "utf-8"
    with open(input_file, "r", encoding=input_encoding) as fid:
        s = csv.Sniffer()
        sniffed_dialect = s.sniff(fid.read(PEEK_SIZE))
    if sniffed_dialect == csv.unix_dialect:
        copyfile(input_file, output_file)
        return
    with open(input_file, "r", encoding=input_encoding) as input_fid:
        r = csv.reader(input_fid, dialect=sniffed_dialect)
        with open(output_file, "w", encoding="utf-8") as output_fid:
            w = csv.writer(output_fid, dialect=csv.unix_dialect)
            w.writerows(r)


def get_drive_id(headers):
    """Get shared drive id.

    Arguments:
        headers {dict} -- request headers

    Returns:
        drive_id {str} -- drive id according of shared project name
    """
    path = "https://graph.microsoft.com/v1.0/me/drive/sharedWithMe"
    response = session.get(path, headers=headers)
    logger.info("Getting shared project id")
    if response.status_code == 200:
        inputs = response.json()
        for v in inputs["value"]:
            if re.match(v["name"], conf["shared_drive"]):
                drive_id = v["remoteItem"]["parentReference"]["driveId"]
    return drive_id


def whitelisted_filename(fname) -> bool:
    """Returns whether filename is whitelisted"""
    # All filenames are whitelisted by default
    if not filename_whitelist:
        return True
    for regex in filename_whitelist:
        if re.match(regex, fname):
            logger.info("File %s is whitelisted by rule '%s'", fname, regex)
            return True
    return False


def blacklisted_filename(fname) -> bool:
    """Returns True if filename appears in the backlist"""
    # No filenames are blacklisted by default
    if not filename_blacklist:
        return False
    for regex in filename_blacklist:
        if re.match(regex, fname):
            logger.info("File %s is blacklisted by rule '%s'", fname, regex)
            return True
    return False


def get_inputs():
    """Get input files from OneDrive"""

    authority = "https://login.microsoftonline.com/{}".format(conf["tenant_id"])

    if conf.get("client_secret"):
        scopes = ["https://graph.microsoft.com/.default"]
        app = msal.ConfidentialClientApplication(
            conf["client_id"],
            authority=authority,
            client_credential=conf["client_secret"],
        )
        token = app.acquire_token_for_client(scopes=scopes)
    else:
        scopes = (
            ["Files.Read", "Files.Read.All"]
            if conf.get("site") is None
            else ["Files.Read", "Files.Read.All", "Sites.Read.All"]
        )
        app = msal.PublicClientApplication(
            conf["client_id"],
            authority=authority,
        )
        token = app.acquire_token_by_username_password(conf["user"], conf["password"], scopes)
    if not "access_token" in token:
        logger.error(token)
    headers = {"Authorization": "Bearer {}".format(token["access_token"])}
    shared_drive = conf.get("shared_drive")
    site = conf.get("site")
    # We need to default to True for backwards compatibility
    merge = conf.get("merge", True)
    if merge is None:
        merge = True
    if not site:
        if not shared_drive:
            input_path = "https://graph.microsoft.com/v1.0/me/drive/root:{input_path}:/children".format(
                input_path=conf["input_path"]
            )
        else:
            # Shared Drive id starts with "b!"
            if shared_drive.startswith("b!"):
                drive_id = shared_drive
                drive_prefix = ""
            else:
                # Search for drive by its name
                drive_id = get_drive_id(headers)
                drive_prefix = "me/"
            input_path = (
                "https://graph.microsoft.com/v1.0/{drive_prefix}drives/{drive_id}/root:{input_path}:/children".format(
                    drive_id=drive_id, input_path=conf["input_path"], drive_prefix=drive_prefix
                )
            )
        response = session.get(input_path, headers=headers)
        response.raise_for_status()
        inputs = response.json()["value"]
    else:
        list_to_table = conf.get("list_to_table", False)
        if list_to_table is None:
            list_to_table = False
        list_id = get_list_id_by_name(site, conf["list_name"], conf.get("subsite"), headers)
        if list_id is None:
            raise ValueError(f"No list was found for name {conf['list_name']}")
        if list_to_table:
            inputs = []
            fname = os.path.join(conf["output_directory"], f"{conf['list_name']}.csv")
            logger.info("Downloading list %s to %s", list_id, fname)
            download_list_as_table(site, list_id, conf.get("subsite"), headers, fname)
        else:
            inputs = get_list_items(site, list_id, conf.get("subsite"), headers)
    try:
        for ins in inputs:
            if blacklisted_filename(ins["name"]) or not whitelisted_filename(ins["name"]):
                logger.warning("Skipping %s - it is either not whitelisted or is blacklisted", ins["name"])
                continue
            if ins["name"].endswith(".xlsx"):
                logger.info("Downloading input xlsx file: %s", ins["name"])
                download_xlsx(ins, headers, merge)
            elif ins["name"].endswith(".csv"):
                logger.info("Downloading input csv file: %s", ins["name"])
                download_csv(ins, headers)
    except Exception as e:
        raise Exception(e)


def in_blacklist(sheet):
    """If sheet is in blacklist returs true.

    Arguments:
        sheet {str} -- name of sheet

    Returns:
        True {bool} -- if sheet is in blacklist, else false
    """
    for word in blacklist:
        if re.match(word, sheet):
            return True
    return False


def in_whitelist(sheet):
    """If sheet is on the whitelist, returns true. Also returns true if no whitelist was provided."""
    if not whitelist:
        return True
    for word in whitelist:
        if re.match(word, sheet):
            return True
    return False


def _is_not_empty(iterable):
    """Helper function that returns False if all members of a iterable are None

    Arguments:
        iterable {Iterable} -- list of values
    """
    for value in iterable:
        if value is not None:
            return True
    return False


def merge_sheets(path, merge=True):
    """Merge all required sheets from excel.

    Arguments:
        path {str} -- path to required excel file
        merge {bool} -- If True, merge all sheets together (default: True)

    Returns:
        sheets_values {list} -- list with rows from all required sheets
    """
    # Oh my God, this is so ugly
    sheets_values = [] if merge else {}
    wb = load_workbook(path, read_only=True, data_only=True)
    basename = os.path.basename(path)
    file_name = basename.split(".")[0]
    for sheet_name in wb.sheetnames:
        if sheet_name in conf.get("sheet_specific_settings", {}).keys():
            _settings = conf["sheet_specific_settings"][sheet_name]
            skpcol = _settings.get("skip_columns", 0)
            tblwdt = _settings.get("table_width", None)
            skprow = _settings.get("skip_rows", 0)
        else:
            skpcol = skip_columns
            tblwdt = table_width
            skprow = skip_rows
        if in_blacklist(sheet_name):
            logger.warning("Skipping blacklisted sheet %s", sheet_name)
            continue
        if not in_whitelist(sheet_name):
            logger.warning("Skipping sheet %s (no on the whitelist)", sheet_name)
            continue
        else:
            logger.info("Processing sheet %s", sheet_name)
            sheet = wb[sheet_name]
            if tblwdt is None:
                table_width_derived = sheet.max_column
                logger.info("Number of columns identified: %d", table_width_derived)
            else:
                table_width_derived = tblwdt
            values = (
                v[skpcol : skpcol + table_width_derived]
                for i, v in enumerate(sheet.values)
                if i >= skprow and _is_not_empty(v)
            )
            columns_names = next(values) + ("file_name", "sheet_name")
            logger.info(f"Extracting following columns {columns_names}")
            values_with_sheet_info = (v + (file_name, sheet_name) for v in values)
            values_as_dict = [
                {name: value[i] for i, name in enumerate(columns_names)} for value in values_with_sheet_info
            ]
            logger.info(
                f"Count of rows extracted (without header and skipped and empty): {len(values_as_dict)} from total {sheet.max_row}"
            )
            # No way to stop now
            if merge:
                sheets_values.extend(values_as_dict)
            else:
                sheets_values.update({sheet_name: values_as_dict})
    return sheets_values


def save_csv_file(csv_file, fieldnames, all_values):
    """Save csv file with all merged excel files.

    Arguments:
        csv_file {str} -- path to csv file

        fieldnames {list} -- list column-names
        all_values {list} -- list with all rows (as a dict) from all excel files
    """
    with open(csv_file, "w", encoding="utf-8") as fid:
        writer = csv.DictWriter(fid, fieldnames=fieldnames, dialect=csv.unix_dialect, extrasaction="ignore")
        writer.writeheader()
        writer.writerows(all_values)


def main():
    """Main function for merging excel files and saving output file."""
    get_inputs()
    all_values = []
    # excel merging
    paths = glob("/tmp/merge/*.xlsx")
    if paths:
        for path in paths:
            logger.info("Merging sheets in file: %s", path)
            all_values.extend(merge_sheets(path))
        output_file = "{}/{}.csv".format(conf["output_directory"], conf["output_file"])
        logger.info("Saving final output file")
        columns_names = list(
            set(conf.get("extract_columns", []) or list(all_values[0].keys()) + ["file_name", "sheet_name"])
        )
        save_csv_file(output_file, columns_names, all_values)
    # csv files
    paths = glob("/tmp/dialect/*.csv")
    for path in paths:
        destination = os.path.join(conf["output_directory"], os.path.basename(path))
        logger.info("Converting %s to UNIX dialect", path)
        convert_dialect(
            path,
            destination,
            input_encoding="utf-8" if not "input_encoding" in conf else conf["input_encoding"],
        )
    # xlsx files not to be merged
    paths = glob("/tmp/convert/*.xlsx")
    for path in paths:
        for sheet_name, content in merge_sheets(path, merge=False).items():
            bname = os.path.splitext(os.path.basename(path))[0]
            destination = os.path.join(conf["output_directory"], f"{bname}_{sheet_name}.csv")
            logger.info("Converting %s:%s to %s", path, sheet_name, destination)
            column_names = list(
                set(conf.get("extract_columns", []) or list(content[0].keys()) + ["file_name", "sheet_name"])
            )
            save_csv_file(destination, column_names, content)
    logger.info("Cleaning up")


if __name__ == "__main__":
    os.makedirs("/tmp/merge", exist_ok=True)
    os.makedirs("/tmp/dialect", exist_ok=True)
    os.makedirs("/tmp/convert", exist_ok=True)
    try:
        main()
    except Exception as error:
        raise
    finally:
        shutil.rmtree("/tmp/merge", ignore_errors=True)
        shutil.rmtree("/tmp/dialect", ignore_errors=True)
        shutil.rmtree("/tmp/convert", ignore_errors=True)
