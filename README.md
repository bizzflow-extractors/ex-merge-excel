# ex-merge-excel

This extractor extracts and merges data from all required excel sheets from multiple excel files (`*.xlsx`) and downloads `*.csv` files from OneDrive storage.

## config.json

_Important note: Diacritics need to be replace by "." and mark start of regex ("^") and end of regex ("$")._

```json
{
  "input_path": "/Rozpocty/2020",
  "output_directory": "/data/out/tables",
  "output_file": "rozpocty",
  "skip_rows": 4,
  "skip_columns": 4,
  "table_width": 4,
  "extract_columns": ["sloupec1", "sloupec2"],
  "blacklist": [
    "^Suma$",
    "^START$",
    "^CIL$",
    "^Pokyny$",
    "..seln.k",
    "^SeznamNS$"
  ],
  "whitelist": ["only-this-sheet", "and-this-one"],
  "password": "",
  "user": "user@host.com",
  "client_id": "",
  "tenant_id": "",
  "input_encoding": "utf-8",
  "shared_drive": null,
  "site": null,
  "subsite": null,
  "list_name": null,
  "merge": true,
  "list_to_table": false,
  "filename_whitelist": ["^file\\d+\\.xlsx$"],
  "filename_blacklist": ["^i-dont-want-you\\.xlsx$"],
  "sheet_specific_settings": {
    "BLD": {
      "skip_columns": 3,
      "table_width": 2
    }
  }
}
```

If parameter `client_secret` is set, extractor will try to authenticate as application, not as user.
Parameters `user` and `password` are then unnecessary and ignored by application.

Note that `input_encoding` and `output_directory` parameters are not required.

If `filename_whitelist` is `null`, not given or an empty array (`[]`) all supported files will be downloaded, otherwise
only files matching one of regexes in the array will be downloaded.

If `filename_blacklist` is `null`, not given or an empty array (`[]`) all supported files will be downloaded, otherwise
only files not matching any of regexes in the array will be downloaded.

If file matches both a regex within `filename_whitelist` and a regex within `filename_blacklist`, it **will not be
downloaded** (blacklist overrides whitelist).

By default, extractor will extract all sheets. If `whitelist` is specified, only sheets matching at least one of
`whitelist` conditions will be extracted. If `blacklist` is specified, only sheets **not matching any
of the conditions** will be extracted. If both `whitelist` and `blacklist` are specified, only sheets matching
at least one of `whitelist` conditions and sheets not matching any of the conditions in `blacklist` will be
extracted.

If `sheet_specific_settings` is given, it is an object with sheet names for keys and `skip_columns`, `skip_rows`
and `table_width` optional parameters. If given, those parameters will be overriden for specified sheet name.

If `shared_drive` in config is not `null` then particular share drive id will be found according it's name and api call specific for this situation will be used.

In case of `null` in `skip_rows` and `skip_columns`, 0 will be used as default value.

In case of `null` in `table_width`, the number of columns will be derived from particular dataset.

All `site`, `subsite` and `list_name` are related to Sharepoint List Items extraction and can be found in your
Sharepoint URL.

If `merge` is omitted, `true` is implied (by default, all excel files will be merged into one).

If `list_to_table` is omitted or `null`, `false` is inferred.

## Local use

```sh
python3 src/main.py
```
