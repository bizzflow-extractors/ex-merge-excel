FROM python:3.7-slim

LABEL com.bizztreat.type="Extractor"
LABEL com.bizztreat.purpose="Bizzflow"
LABEL com.bizztreat.component="ex-merge-excel"
LABEL com.bizztreat.title="Merge Excel extractor"

VOLUME /data/out/tables
VOLUME /config

ADD requirements.txt /requirements.txt

RUN python -m pip install --upgrade pip setuptools requests
RUN python -m pip install -r /requirements.txt

ADD src/ /code
WORKDIR /code

# ENTRYPOINT ["python", "-u", "main.py"]
CMD rm -rf /tmp/* ; python -u main.py
